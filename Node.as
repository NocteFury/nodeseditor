﻿package  {
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.display.Sprite;
	import flash.display.Graphics;
	import flash.display.Stage;
	import fl.motion.Color;
	import flash.geom.Point;
	
	public class Node extends Sprite {
		
		// Идентификаторы сторон узла
		public static const RIGHT_SIDE = 0;
		public static const TOP_SIDE = 1;
		public static const LEFT_SIDE = 2;
		public static const BOTTOM_SIDE = 3;
		
		// Флаг выделен ли данный узел, или нет
		private var selected: Boolean = false;
		
		// Цвет закраски узла
		private var color: uint = 0;
		
		// Размеры и положение узла
		public var rect: Rectangle;
		
		// Массив соединений, которые добавлены к этому узлу
		private const connections: Array = new Array();

		// Класс узла. ПОзволяет обновлять местоположение точек соединения и хранит информацию о своей форме
		// rect - положение и размеры узла
		// color - цвет закраски узла
		public function Node(rect: Rectangle, color: uint) {
			this.rect = rect;
			this.color = color;
			this.move(rect.x, rect.y);
			
			draw();
		}
		
		// Добавляет соединение к узлу. Необходимо для того
		// чтобы положение и форма соединения обновлялась, если
		// преремещается подключенный к ней узел
		public function addConnection(connection: Connection) {
			connections.push(connection);
		}
		
		// Проверяет, было ли соединение между текущим узлом и переданным
		public function isConnected(node: Node) {
			for each (var connection in connections) {
				if (connection.from == node || connection.to == node) return true;
			}
			return false;
		}
		
		// Выделяет узел красной границей
		public function select() {
			if (!selected) {
				selected = true;
				draw();
			}
		}
		
		// Снимает выделение с узла
		public function deselect() {
			if (selected) {
				selected = false;
				draw();
			}
		}
		
		// Возвращает линию указанной стороны
		public function getSide(side: Number): Line {
			switch (side) {
				case TOP_SIDE: return new Line(new Point(this.x, this.y), new Point(this.x + this.width, this.y));
				case RIGHT_SIDE: return new Line(new Point(this.x + this.width, this.y), new Point(this.x + this.width, this.y + this.height));
				case BOTTOM_SIDE: return new Line(new Point(this.x, this.y + this.height), new Point(this.x + this.width, this.y + this.height));
				case LEFT_SIDE: return new Line(new Point(this.x, this.y), new Point(this.x, this.y + this.height));
			}
			return null;
		}
		
		// Перемещает узел в указанные координаты
		// Обновляет местоположение соединений, добавленных к этому узлу
		public function move(x: Number, y: Number) {
			if (this.x == x && this.y == y) return;
			
			this.x = this.rect.x = x;
			this.y = this.rect.y = y;
			
			for each (var connection in this.connections) connection.update();
		}
		
		private function draw() {
			graphics.clear();
			
			// Если узел был выделен, создаем обводку
			if (this.selected) graphics.lineStyle(2, 0xFF0000);
			
			graphics.beginFill(color);
			graphics.drawRect(0, 0, rect.width, rect.height);
			graphics.endFill();
		}
		
	}
	
}
