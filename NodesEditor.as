﻿package  {
	import flash.display.Stage;
	import flash.geom.Rectangle;
	import flash.geom.Point;
	import flash.events.MouseEvent;
	import flash.display.Shape;
	
	// Основной класс обработки
	public class NodesEditor {

		// Размеры прямоугольника
		private static const RECT_WIDTH: Number = 100; // Ширина
		private static const RECT_HEIGHT: Number = 50; // Высота

		private var stage: Stage;			// Ссылка на Stage
		private var stageRect: Rectangle;	// Размеры stage (кэшированные)
		
		// Список созданных узлов
		private const nodes: Array = new Array();
		
		// Метаданные drag'n'drop
		private const pivot: Point = new Point();	// Относительная точка перетаскивания
		private var grabbed: Node;					// Текущий перетаскиваемый элемент
		
		private var selected: Node;

		// stage (Stage) - ссылка на Stage проекта
		public function NodesEditor(stage: Stage) {
			// Сохраняем ссылку на Stage и кэшируем его размеры (в Rectangle)
			this.stage = stage;
			this.stageRect = new Rectangle(0, 0, stage.stageWidth, stage.stageHeight);
			
			// Разрешаем использование двойного клика и подписываемся на его событие
			stage.doubleClickEnabled = true;			
			stage.addEventListener(MouseEvent.DOUBLE_CLICK, onCreateNode);
			
			// Обработка перетаскивания узлов
			stage.addEventListener(MouseEvent.MOUSE_MOVE, onMove);
			stage.addEventListener(MouseEvent.MOUSE_UP, function () { grabbed = null; });
			
			// Очистка выделения узла
			stage.addEventListener(MouseEvent.CLICK, function () { deselect(); });
		}
		
		// Создает узел по указанным координатам
		// Возвращает узел, если он был успешно создан, иначе null
		public function create(x: Number, y: Number): Node {
			const position = this.clamp(x, y, RECT_WIDTH, RECT_HEIGHT, this.stageRect);
			const rect = new Rectangle(position.x, position.y, RECT_WIDTH, RECT_HEIGHT);
			
			// Проверяем, есть ли свободное пространство для узла, если есть, то создаем его
			if (this.isSpaceFree(rect)) return new Node(rect, Math.random() * 0xFFFFFF);
			
			// Иначе, возвращаем null
			return null;
		}
		
		public function moveTo(target: Node, dest: Point) {
			// Ограничиваем по размерам Stage.
			const position = this.clamp(dest.x, dest.y, RECT_WIDTH, RECT_HEIGHT, this.stageRect);
			const rect = new Rectangle(position.x, position.y, RECT_WIDTH, RECT_HEIGHT);
			
			// Проверяем, пересекается ли наш узел с кем либо, если будет перемещен к выбранной области
			var intersects = false;
			
			for each (var node in nodes) {
				if (node != target && !node.rect.intersection(rect).isEmpty()) {
					intersects = true;
					break;
				}
			}
			
			// Проверяем, было ли пересечение
			if (!intersects) {
				// Если нет, то перемещаем узел к указанным координатам
				target.move(position.x, position.y);
			} else {
				// Если да, то пытаемся найти ближайшее смещение, до пересечения с другим узлом
				const nearest = getMaxMovement(target, position);
				
				// Получаем вектор движения
				const dir = position.subtract(new Point(target.x, target.y));
				
				// Проверяем, была ли найдена точка пересечения хотя бы с одной фигурой
				// TODO: плавающий баг. Иногда в данном случае код возвращает Infinity
				// что означает, что узел при перемещении не сталкивается с другим узлом,
				// однако код выше говорит обратное.
				if (nearest != Infinity) {
					// Проверяем, есть ли ограничение движения, если есть, уменьшаем длину вектора движения
					// на максимально допустимое
					if (nearest < dir.length) dir.normalize(nearest);
					
					// Двигаем узел по полученному вектору движения
					target.move(target.x + dir.x, target.y + dir.y);
				}
			}
		}
		
		// Возвращает true, если по указанным координатам можно расположить прямоугольник, иначе false
		public function isSpaceFree(rect: Rectangle): Boolean {
			// Проверяем, занято ли это место
			for each (var node in nodes) {
				if (!node.rect.intersection(rect).isEmpty()) return false;
			}
			
			// Возвращаем true, если место свободно
			return true;
		}
		
		// Возвращает максимальное расстояние, на которое может быть передвинут узел
		// до столкновения с ближайшим другим узлом
		private function getMaxMovement(target: Node, dest: Point): Number {
			// Получаем вектор направления движения
			const dir = new Point(dest.x - target.x, dest.y - target.y);
			dir.normalize(1);
			
			// Получаем минимальное из всех возможных пересечений
			return Math.min(
				getNearestIntersection(target, new Ray(new Point(target.x, target.y), dir)),
				getNearestIntersection(target, new Ray(new Point(target.x, target.y + target.height), dir)),
				getNearestIntersection(target, new Ray(new Point(target.x + target.width, target.y), dir)),
				getNearestIntersection(target, new Ray(new Point(target.x + target.width, target.y + target.height), dir))
			);
		}
		
		// Возвращает ближайшую точку пересечения указанным лучом до другого узла
		private function getNearestIntersection(target: Node, ray: Ray): Number {
			var shortDistance: Number = Infinity;
			
			for each (var node in nodes) {
				// Пропускаем исходный узел
				if (node == target) continue;
				
				// Если фигура уже пересекается с другим узлом, возвращаем 0.
				if (!node.rect.intersection(target.rect).isEmpty()) return 0;
				
				// Получаем стороны, которые в принципе могут быть задеты лучом
				// (так экономится минимум в 2 раза больше производительности)
				const sides = this.getTargetSides(node, ray.dir);
				for each (var side in sides) {
					// Проверяем, пересекается ли луч с текущей стороной
					const intersection = ray.raycast(side);
					if (intersection) {
						// Если да, то получаем длину точки пересечения и начала луча
						const distance = Point.distance(ray.origin, intersection);
						
						// Если она меньше, сохраняем эту точку и ее длину
						if (Math.min(shortDistance, distance) != shortDistance) shortDistance = distance;
					}
				}
			}
			
			// Возвращаем минимальную дистанцию
			return shortDistance;
		}
		
		// Возаращает новую точку со смещенными значениями, для того, чтобы прямоугольник находился в указанных границах
		private function clamp(x: Number, y: Number, width: Number, height: Number, bounds: Rectangle): Point {
			// Смещаем координаты в соответствии с зоной пересечения
			const position = new Point(
				Math.max(0, Math.min(bounds.width - width, x)),
				Math.max(0, Math.min(bounds.height - height, y))
			);
			
			return position;
		}
		
		// Возвращает массив сторон, которые могут быть затронуты переданным лучом
		private function getTargetSides(node: Node, dir: Point): Array {
			const sides = new Array();
			
			if (dir.x > 0) sides.push(node.getSide(Node.LEFT_SIDE));
			if (dir.y > 0) sides.push(node.getSide(Node.TOP_SIDE));
			
			if (dir.x < 0) sides.push(node.getSide(Node.RIGHT_SIDE));
			if (dir.y < 0) sides.push(node.getSide(Node.BOTTOM_SIDE));
			
			return sides;
		}
		
		// ==================================== Обработчики событий ====================================
		
		// Обработчик события двойного клика (создание узла)
		private function onCreateNode(event: MouseEvent) {
			// Смещаем координаты, чтобы прямоугольник создавался центром в точке указанной мышкой
			const x = event.stageX - RECT_WIDTH / 2;
			const y = event.stageY - RECT_HEIGHT / 2;
			
			// Пробуем создать узел по заданным координатам
			const node = this.create(x, y);
			
			// Если это возможно и узел создался, то...
			if (node) {
				// Сохраняем его в списке созданных
				nodes.push(node);
				
				// Подписываемся на событие, чтобы отмечать прямоугольник, как выбранный для перемещения
				node.addEventListener(
					MouseEvent.MOUSE_DOWN,
					function (event: MouseEvent) {
						// Сохраняем относительную позицию точки удержания
						pivot.x = event.localX;
						pivot.y = event.localY;
						
						// Сохраняем текущее звено, как выбранное для перемещения
						grabbed = node;
					}
				);
				
				// Подписываемся на правую кнопку мыши, для выделения узла (необходимо для создание связей между узлами)
				node.addEventListener(MouseEvent.RIGHT_CLICK, function () { select(node); });
				
				// Добавляем узел на Stage
				this.stage.addChild(node);
			}
		}
		
		// Выделяет узел для соединения
		private function select(node: Node) {
			// Если узла выделено не было, то выделяем его
			if (selected == null) {
				selected = node;
				selected.select();
			} else {
				// Если же узел был выбран и выбран другой узел, а так же между ними еще не было связи
				if (selected != node && !selected.isConnected(node)) {
					// Создаем связь между ними
					selected.stage.addChildAt(new Connection(selected, node), 0);
				}
				
				// Снимаем выделение с узела
				deselect();
			}
		}
		
		// Снимает выделение с узла
		private function deselect() {
			// Если был выделен узел...
			if (selected != null) {
				selected.deselect();
				selected = null;
			}
		}
		
		// Обработчик события перемещения мыши (обработка перемещения объекта)
		private function onMove(event: MouseEvent) {
			if (grabbed) this.moveTo(grabbed, new Point(event.stageX - pivot.x, event.stageY - pivot.y));
		}
		
	}
	
}
