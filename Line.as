﻿package  {
	import flash.geom.Point;
	
	public class Line {

		public var a1: Point;
		public var a2: Point;

		// Класс-обертка для указания двух точек, составляющие линию
		public function Line(a1: Point, a2: Point) {
			this.a1 = a1;
			this.a2 = a2;
		}

	}
	
}
