﻿package  {
	import flash.display.Sprite;
	
	public class Connection extends Sprite {

		private var _from: Node;
		private var _to: Node;
		
		public function get from() { return _from; }
		public function get to() { return _to; }

		// Класс соединения. Позволяет хранить узлы соединения и показывать линию между ними
		public function Connection(from: Node, to: Node) {
			this._from = from;
			this._to = to;
			
			// Сохраняем, что данное соединение добавлено к указанным узлам
			this.from.addConnection(this);
			this.to.addConnection(this);
			
			this.update();
		}
		
		public function update() {
			this.graphics.clear();
			this.graphics.lineStyle(1, 0x000000);
			this.graphics.moveTo(from.x + from.width / 2, from.y + from.height / 2);
			this.graphics.lineTo(to.x + to.width / 2, to.y + to.height / 2);
		}

	}
	
}
