﻿package  {
	import flash.geom.Point;
	
	public class Ray {

		public var origin: Point;
		public var dir: Point;

		// Класс луча. Необходимо для проведения raycast-а к линиям
		// origin - точка начала луча
		// dir - направление луча
		public function Ray(origin: Point, dir: Point) {
			this.origin = origin;
			this.dir = dir;
		}
		
		// Испускает луч до линии. Если луч и линия пересекаются
		public function raycast(line: Line): Point {
			// Если начало луча находится непосредственно на линии, то возвращаем ее.
			if (this.isOnLine(origin, line)) return origin;
			
			// Получаем точку перемечения луча и линии
			const intersection = this.intersects(origin, origin.add(dir), line.a1, line.a2);
			
			// Если пересечения не было, возвращаем null.
			if (!intersection) return null;
			
			// Если пересечение было, получаем вектор направления к точке пересечения
			const dest = intersection.subtract(origin);
			dest.normalize(1);		
			
			// Округляем значения для проверки на соответствие направлению (точность до сотых)
			dest.x = Math.round(dest.x * 100) / 100;
			dest.y = Math.round(dest.y * 100) / 100;
			
			dir.x = Math.round(dir.x * 100) / 100;
			dir.y = Math.round(dir.y * 100) / 100;
			
			// Если точка пересечения на указанной линии и она находится по направлению луча, то возвращаем эту точку пересечения, иначе null
			return dest.equals(dir) && this.isOnLine(intersection, line) ? intersection : null;
		}
		
		// Проверяет, находится ли точка на указанной линии
		private function isOnLine(point: Point, line: Line) {
			return Point.distance(line.a1, point) + Point.distance(point, line.a2) == Point.distance(line.a1, line.a2);
		}

		// Возвращает точку пересечения двух прямых. Возвращает null, если прямые параллельны
		private function intersects(a: Point, b: Point, c: Point, d: Point): Point {
			const a1 = b.y - a.y; 
			const b1 = a.x - b.x; 
			const c1 = a1 * a.x + b1 * a.y; 
		   
			const a2 = d.y - c.y; 
			const b2 = c.x - d.x; 
			const c2 = a2 * c.x + b2 * c.y; 
		   
			const determinant = a1 * b2 - a2 * b1; 
		   
			if (determinant == 0) { 
				return null; 
			} else {
				const x = (b2 * c1 - b1 * c2) / determinant; 
				const y = (a1 * c2 - a2 * c1) / determinant; 
				
				return new Point(x, y); 
			} 
		}

	}
	
}
